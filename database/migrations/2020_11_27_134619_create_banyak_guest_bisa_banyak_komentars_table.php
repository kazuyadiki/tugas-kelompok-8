<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanyakGuestBisaBanyakKomentarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banyak_guest_bisa_banyak_komentars', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('komentar_id');
            $table->foreign('komentar_id')->references('id')->on('komentars');

            $table->unsignedBigInteger('guest_id');
            $table->foreign('guest_id')->references('id')->on('guests');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banyak_guest_bisa_banyak_komentars');
    }
}
