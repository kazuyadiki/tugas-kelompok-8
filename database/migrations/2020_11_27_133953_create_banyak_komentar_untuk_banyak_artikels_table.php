<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanyakKomentarUntukBanyakArtikelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banyak_komentar_untuk_banyak_artikels', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('artikel_id');
            $table->foreign('artikel_id')->references('id')->on('artikel');

            $table->unsignedBigInteger('komentar_id');
            $table->foreign('komentar_id')->references('id')->on('komentars');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banyak_komentar_untuk_banyak_artikels');
    }
}
