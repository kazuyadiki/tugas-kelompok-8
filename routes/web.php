<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/tes', function () {
    return view('master');
});

//Route untuk Menampilkan Home dan Artikel

Route::get('/', function () {
    return view('hometes');
});

Route::get('/post', function () {
    return view('post');
});

Route::get('/macchupichu', function () {
    return view('macchupichu');
});

Route::get('/jeonju', function () {
    return view('jeonju');
});

Route::get('/kursunlu waterfall', function () {
    return view('kursunlu_waterfall');
});

Route::get('/plitvice lake', function () {
    return view('plitvice_lake');
});


Route::resource('artikel', 'BlogController');
//Route::get('/artikel/create', 'BlogController@create');
//Route::post('/artikel', 'BlogController@store');
//Route::get('/artikel', 'BlogController@index');
//Route::get('/artikel/{id}', 'BlogController@show');

Route::get('/welcome', 'AwalController@welcome');


Auth::routes();



Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
