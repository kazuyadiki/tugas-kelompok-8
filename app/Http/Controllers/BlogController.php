<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Blog;
use RealRashid\SweetAlert\Facades\Alert;

class BlogController extends Controller
{

    public function __construct(){
        $this->middleware('auth')->only(['create', 'store', 'edit', 'update', 'delete']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$artikel = DB::table('artikel')->get();
        //dd($posts);
        $artikel = Blog::all();
        return view('blog.index', compact('artikel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:artikel',
            'isi' => 'required',
        ]);
        
        //$query = DB::table('artikel')->insert([
        //    "judul" => $request["judul"],
        //    "isi" => $request["isi"],
        //]);

        //$blog = new Blog;
        //$blog->judul = $request['judul'];
        //$blog->isi = $request['isi'];
        //$blog->save();

        $blog = Blog::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

         Alert::success('Berhasil', 'Berhasil Menambahkan Artikel Baru');
            
         return redirect('/artikel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$artike = DB::table('artikel')->where('id', $id)->first();
        $artike = Blog::find($id);
        return view('blog.show', compact('artike'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$artike = DB::table('artikel')->where('id', $id)->first();
        $artike = Blog::find($id);
        return view('blog.edit', compact('artike'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|unique:artikel',
            'isi' => 'required',
        ]);

        /*$query = DB::table('artikel')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);*/

        $update = Blog::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

            Alert::success('Berhasil', 'Berhasil Update Artikel');
            return redirect('/artikel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$query = DB::table('artikel')->where('id', $id)->delete();
        Blog::destroy($id);
        Alert::success('Berhasil', 'Berhasil Menghapus Artikel');
        return redirect('/artikel');
    }
}
