<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Blog;
use RealRashid\SweetAlert\Facades\Alert;

class AwalController extends Controller
{
    public function welcome()
    {
    $artikel = DB::table('artikel')->get();
        //dd($posts);
    return view('blog.indexAwal', compact('artikel'));
    }
}