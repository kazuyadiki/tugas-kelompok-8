<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = "artikel";

    //protected $fillable = ["judul"];
    protected $guarded = [];
}
