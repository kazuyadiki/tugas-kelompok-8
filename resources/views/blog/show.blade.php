@extends('master')

@push('style')
<link href="{{asset('/blog/bootstrap/komen/komen.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <h2>{{ $artike->judul }}</h2>
        <p>{!! $artike->isi !!}</p>
      </div>
    </div>
  </div>
  @endsection

  @section('comment')
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="card">
                        <div class="p-3">
                            <h6>Comments</h6>
                        </div>
                        <form role="form" action="/artikel" method="POST">
                            @csrf
                        <div class="mt-3 d-flex flex-row align-items-center p-3 form-color">
                        <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter your comment...">
                    </form>
                    <div class="mt-2">
                        <div class="d-flex flex-row p-3">
                            <div class="w-100">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex flex-row align-items-center"> <span class="mr-2">Brian selter</span> </div> <small>12h ago</small>
                                </div>
                                <p class="text-justify comment-text mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  @endsection